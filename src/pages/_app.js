import Head from "next/head"
import {DefaultSeo} from "next-seo"
import {ChakraProvider, extendTheme} from "@chakra-ui/react"
import "@fontsource/inter"

const theme = extendTheme({
  fonts: {
    heading: "Inter",
    body: "Inter",
  },
})

const seo = {
  title: "Celebrations",
  titleTemplate: "%s - Celebrations",
  description: "Beautiful digital celebration cards.",
  openGraph: {
    url: "https://happy-birthday-jet.vercel.app/",
    title: "Celebrations",
    description: "Beautiful digital celebration cards.",
  },
}

function App({Component, pageProps}) {
  return (
    <>
      <Head>
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png"/>
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png"/>
        <meta content='width=device-width, initial-scale=1' name='viewport' />
      </Head>
      <DefaultSeo {...seo}/>
      <ChakraProvider theme={theme}>
        <Component {...pageProps} />
      </ChakraProvider>
    </>
  )
}

export default App
