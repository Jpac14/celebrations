import {useRouter} from "next/router"
import FathersDayTemplate from "../../components/FathersDayTemplate"
import {Text, VStack, Center, Image} from "@chakra-ui/react"

export default function Dad() {
  const {to, from, imageUrl} = useRouter().query

  return (
    <FathersDayTemplate to={to} imageUrl={imageUrl}>
      <VStack textColor="black">
        <Text>Thank you for always supporting me thoughout life.</Text>
        <Text>Thank you for all the advice you have given me.</Text>
        <Text>But, It&apos;s your turn to be spoilt today!</Text>
        <Text>Love from {from} ❤️</Text>
      </VStack>
      <Center mt={4}>
        <Image alt={to} h="3xs" src={imageUrl} borderRadius="lg" shadow="lg" />
      </Center>
    </FathersDayTemplate>
  )
}
