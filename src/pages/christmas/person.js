import {useRouter} from "next/router"
import ChristmasTemplate from "../../components/ChristmasTemplate"
import {Text, Avatar, VStack} from "@chakra-ui/react"

export default function Person() {
  const {to, from, imageUrl} = useRouter().query

  return (
    <ChristmasTemplate to={to} imageUrl={imageUrl}>
      <VStack>
        <Text>May your Christmas sparkle with moments of love, laughter and goodwill.</Text>
        <Text>May the year ahead be full of contentment and joy.</Text>
        <Text>From {from}</Text>
      </VStack>
      <Avatar size="xl" mt={4} name={to} src={imageUrl} />
    </ChristmasTemplate>
  )
}
