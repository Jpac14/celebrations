import {useRouter} from "next/router"
import ChristmasTemplate from "../../components/ChristmasTemplate"
import {Text, Center, Image, VStack} from "@chakra-ui/react"

export default function Family() {
  const {to, from, imageUrl} = useRouter().query

  return (
    <ChristmasTemplate to={to} imageUrl={imageUrl}>
      <VStack>
        <Text>May your Christmas sparkle with moments of love, laughter and goodwill.</Text>
        <Text>May the year ahead be full of contentment and joy.</Text>
        <Text>Love from {from} ❤️</Text>
      </VStack>
      <Center mt={4}>
        <Image alt={to} h="3xs" src={imageUrl} borderRadius="lg" shadow="lg" />
      </Center>
    </ChristmasTemplate>
  )
}
