import {useRouter} from "next/router"
import MothersDayTemplate from "../../components/MothersDayTemplate"
import {Text, VStack, Center, Image} from "@chakra-ui/react"

export default function Mum() {
  const {to, from, imageUrl} = useRouter().query

  return (
    <MothersDayTemplate to={to} imageUrl={imageUrl}>
      <VStack textColor="black">
        <Text>Thank you for always supporting me thoughout life.</Text>
        <Text>It&apos;s your turn to be spoilt today!</Text>
        <Text>Love from {from} ❤️</Text>
      </VStack>
      <Center mt={4}>
        <Image alt={to} h="3xs" src={imageUrl} borderRadius="lg" shadow="lg" />
      </Center>
    </MothersDayTemplate>
  )
}
