import {useRouter} from "next/router"
import BirthdayTemplate from "../../components/BirthdayTemplate"
import {Text, VStack} from "@chakra-ui/react"

export default function Sister() {
  const {to, from, imageUrl} = useRouter().query

  return (
    <BirthdayTemplate to={to} imageUrl={imageUrl}>
      <VStack>
        <Text>I hope you have wonderful day and all your wishes come true.</Text>
        <Text>Despite our fights, I always have your back. I love you {to}.</Text>
        <Text>Love from {from} ❤️</Text>
      </VStack>
    </BirthdayTemplate>
  )
}
