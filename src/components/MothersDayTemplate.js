import Template from "./Template"
import {chakra, Heading} from "@chakra-ui/react"
import {NextSeo} from "next-seo"

const gradientColors = [
  "EA698B",
  "D55D92",
  "C05299",
  "AC46A1",
  "973AA8",
  "822FAF",
  "6D23B6",
  "6411AD",
  "571089",
  "47126B",
]

export default function MothersDay({to, imageUrl, children}) {
  const gradient = `linear(to-l, ${gradientColors
    .map((color) => ("#" + color).toLocaleLowerCase())
    .join(", ")})`

  return (
    <>
      <NextSeo
        title={`Happy Mother's Day ${to}`}
        description={`Happy Mother's Day ${to}`}
        openGraph={{images: [{url: imageUrl, alt: to}]}}
      />
      <Template
        heading={
          <Heading bgGradient={gradient} bgClip="text">
            Happy Mother&apos;s Day, {to} <chakra.span color="black">💕</chakra.span>
          </Heading>
        }
        particles={particles}
      >
        {children}
      </Template>
    </>
  )
}

const particles = {
  fpsLimit: 120,
  background: {
    color: "#fff",
  },
  particles: {
    number: {
      value: 50,
    },
    color: {
      value: gradientColors,
    },
    shape: {
      type: "circle",
    },
    opacity: {
      value: 0.5,
    },
    size: {
      value: {min: 200, max: 400},
    },
    move: {
      enable: true,
      angle: {
        value: 30,
        offset: 0,
      },
      speed: {
        min: 10,
        max: 20,
      },
      direction: "top",
      outModes: {
        default: "destroy",
        bottom: "none",
      },
    },
  },
  detectRetina: true,
  emitters: {
    position: {
      x: 50,
      y: 150,
    },
    rate: {
      delay: 0.2,
      quantity: 3,
    },
    size: {
      width: 100,
      height: 50,
    },
  },
}
