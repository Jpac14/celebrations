import {css, keyframes} from "@emotion/react"
import {Heading, chakra} from "@chakra-ui/react"
import {NextSeo} from "next-seo"
import Template from "./Template"

export default function ChristmasTemplate({to, imageUrl, children}) {
  const colorA = "#42b883"
  const colorB = "#fe346e"

  const move = keyframes`
    from {
      background-position: 0px;
    }
    
    to {
      background-position: 1000px;
    } 
  `

  return (
    <>
      <NextSeo
        title={`Merry Christmas ${to}`}
        description={`Merry Christmas ${to}`}
        openGraph={{images: [{url: imageUrl, alt: to}]}}
      />
      <Template
        heading={
          <>
            <Heading
              css={css`
                background: repeating-linear-gradient(
                  45deg,
                  ${colorA},
                  ${colorA} 30px,
                  ${colorB} 30px,
                  ${colorB} 60px
                );
                background-clip: text;
                color: transparent;
                animation: 40s linear infinite ${move};
              `}
            >
              Merry Christmas {to} <chakra.span color="black">🎄</chakra.span>
            </Heading>
          </>
        }
        particles={particles}
      >
        {children}
      </Template>
    </>
  )
}

const particles = {
  background: {
    color: "#000000",
  },
  particles: {
    color: {
      value: "#fff",
    },
    move: {
      direction: "bottom",
      enable: true,
      outModes: "out",
      speed: 0.1,
    },
    number: {
      density: {
        enable: true,
        area: 800,
      },
      value: 100,
    },
    opacity: {
      value: 0.7,
    },
    shape: {
      type: "circle",
    },
    size: {
      value: 10,
    },
    wobble: {
      enable: true,
      distance: 10,
      speed: 10,
    },
    zIndex: {
      value: {
        min: 0,
        max: 100,
      },
    },
  },
}
