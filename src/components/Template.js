import {Center, Box, Flex, HStack, Tag, Link} from "@chakra-ui/react"
import dynamic from "next/dynamic"

const DynamicParticleBackground = dynamic(() => import("./ParticleBackground"), {
  ssr: false,
})

export default function Template({heading, particles, children}) {
  return (
    <>
      <Center position="absolute" zIndex="overlay" h="100vh" w="100vw">
        <Center bg="white" p={8} borderRadius={["none", "lg"]} boxShadow="lg">
          <Box userSelect="none" textAlign="center" fontSize="lg">
            <Box pb={4}>{heading}</Box>
            {children}
          </Box>
        </Center>
      </Center>
      <DynamicParticleBackground options={particles} />
      <Flex position="absolute" flexDirection="column-reverse" h="100vh">
        <HStack p={1} h={10} zIndex="overlay">
          <Tag h="100%" colorScheme="blue">
            <Link href="https://gitlab.com/Jpac14" isExternal>
              Built by Jpac14 👨‍💻
            </Link>
          </Tag>
        </HStack>
      </Flex>
    </>
  )
}
