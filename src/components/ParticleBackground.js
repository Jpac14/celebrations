import Particles from "react-tsparticles"

export default function ParticleBackground({options}) { 
  return <Particles options={options} />
}
